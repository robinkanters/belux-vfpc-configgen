Config generator for Belux vACC VFPC Config
===========================================

Download latest config: [Sids.json](https://gitlab.com/robinkanters/belux-vfpc-configgen/-/jobs/artifacts/master/raw/Sids.json?job=run)

Download plugin: [Github](https://github.com/hpeter2/VFPC/releases)

Installation instructions: [On Github](https://github.com/hpeter2/VFPC#how-to-use)
