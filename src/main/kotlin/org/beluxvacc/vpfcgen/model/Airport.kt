package org.beluxvacc.vpfcgen.model

import kotlinx.serialization.Serializable
import org.beluxvacc.vpfcgen.api.VfpcConfigDsl
import org.beluxvacc.vpfcgen.internal.EnumSerialNameSerializer
import org.beluxvacc.vpfcgen.internal.EnumSerialNameSerializer.*

@Serializable(with = Airport.AirportSerializer::class)
enum class Airport {
    @SerialEnumName("EBBR")
    @VfpcConfigDsl
    BRUSSELS,

    @SerialEnumName("EBCI")
    @VfpcConfigDsl
    CHARLEROI,

    @SerialEnumName("EBAW")
    @VfpcConfigDsl
    ANTWERP,

    @SerialEnumName("EBLG")
    @VfpcConfigDsl
    LIEGE,

    @SerialEnumName("EBOS")
    @VfpcConfigDsl
    OSTEND,

    @SerialEnumName("ELLX")
    @VfpcConfigDsl
    LUXEMBOURG,
    ;
    object AirportSerializer : EnumSerialNameSerializer<Airport>(Airport::class)
}
