package org.beluxvacc.vpfcgen.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class AirportConfig(
    @SerialName("icao")
    val airport: Airport,
    @SerialName("sids")
    val initialWaypoints: Map<String, List<Restriction>>,
)
