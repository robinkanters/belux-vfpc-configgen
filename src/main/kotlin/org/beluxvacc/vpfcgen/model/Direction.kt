package org.beluxvacc.vpfcgen.model

import kotlinx.serialization.Serializable
import org.beluxvacc.vpfcgen.api.VfpcConfigDsl
import org.beluxvacc.vpfcgen.internal.EnumSerialNameSerializer
import org.beluxvacc.vpfcgen.model.Direction.*

@Serializable(with = DirectionSerializer::class)
enum class Direction {
    @VfpcConfigDsl ODD,
    @VfpcConfigDsl EVEN,
    ;
    object DirectionSerializer : EnumSerialNameSerializer<Direction>(Direction::class)
}
