package org.beluxvacc.vpfcgen.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Restriction(
    val direction: Direction? = null,
    @SerialName("min_fl")
    val minFl: Int? = null,
    @SerialName("max_fl")
    val maxFl: Int? = null,
    val destinations: List<String>? = null,
    val airways: List<String>? = null,
    @SerialName("engine")
    val engines: List<Engine>? = null,
)
