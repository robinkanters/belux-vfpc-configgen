package org.beluxvacc.vpfcgen.model

import kotlinx.serialization.Serializable
import org.beluxvacc.vpfcgen.internal.EnumSerialNameSerializer
import org.beluxvacc.vpfcgen.internal.EnumSerialNameSerializer.*
import org.beluxvacc.vpfcgen.model.Engine.*

@Serializable(with = EngineSerializer::class)
enum class Engine {
    @SerialEnumName("P")
    PISTON,

    @SerialEnumName("T")
    TURBOPROP,

    @SerialEnumName("J")
    JET,

    @SerialEnumName("E")
    ELECTRIC,

    @SerialEnumName("?")
    UNKNOWN,
    ;
    object EngineSerializer : EnumSerialNameSerializer<Engine>(Engine::class)

    companion object {
        val All = values().toList() - UNKNOWN
    }
}
