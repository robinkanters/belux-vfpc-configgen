package org.beluxvacc.vpfcgen.internal

import org.beluxvacc.vpfcgen.api.VfpcConfigDsl
import org.beluxvacc.vpfcgen.model.Airport
import org.beluxvacc.vpfcgen.model.AirportConfig
import org.beluxvacc.vpfcgen.model.Restriction

@VfpcConfigDsl
class AirportContext(internal val airport: Airport) {
    internal val sids = mutableMapOf<String, List<Restriction>>()

    @VfpcConfigDsl
    fun sids(name: String, block: (SidContext.() -> Unit)? = null) {
        sids[name] = SidConfigBuilder().build(SidContext().apply { block?.invoke(this) })
    }

    @VfpcConfigDsl
    operator fun String.invoke(block: (SidContext.() -> Unit)? = null) = sids(this, block)
}

class AirportConfigBuilder {
    fun build(context: AirportContext) = AirportConfig(context.airport, context.sids)
}
