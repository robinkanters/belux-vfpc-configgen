package org.beluxvacc.vpfcgen.internal

import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlin.reflect.KClass

abstract class EnumSerialNameSerializer<E : Enum<E>>(private val kClass: KClass<E>) : KSerializer<E> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("EnumSerialNameSerializer", PrimitiveKind.STRING)

    override fun serialize(encoder: Encoder, value: E) {
        val enumFieldAnnotation = value.getEnumFieldAnnotation<SerialEnumName>()
        encoder.encodeString(enumFieldAnnotation?.value ?: value.name)
    }

    override fun deserialize(decoder: Decoder) = throw NotImplementedError("Not supported")

    private inline fun <reified A : Annotation> Enum<*>.getEnumFieldAnnotation(): A? =
        kClass.java.getField(name).getAnnotation(A::class.java)

    @Retention(AnnotationRetention.RUNTIME)
    annotation class SerialEnumName(val value: String)
}
