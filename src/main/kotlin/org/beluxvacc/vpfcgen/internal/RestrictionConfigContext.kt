package org.beluxvacc.vpfcgen.internal

import org.beluxvacc.vpfcgen.api.VfpcConfigDsl
import org.beluxvacc.vpfcgen.model.Direction
import org.beluxvacc.vpfcgen.model.Engine
import org.beluxvacc.vpfcgen.model.Restriction

@VfpcConfigDsl
class RestrictionConfigContext {
    @VfpcConfigDsl
    var direction: Direction? = null
    @VfpcConfigDsl
    var minFl: Int? = null
    @VfpcConfigDsl
    var maxFl: Int? = null
    @VfpcConfigDsl
    var cruisingFl: IntRange? = null
    @VfpcConfigDsl
    internal var destinations: List<String>? = null
    @VfpcConfigDsl
    internal var airways: List<String>? = null
    @VfpcConfigDsl
    internal var engines: List<Engine> = Engine.All

    @VfpcConfigDsl
    fun destinations(vararg names: String) {
        destinations = names.toList()
    }

    @VfpcConfigDsl
    fun airways(vararg names: String) {
        airways = names.toList()
    }

    @VfpcConfigDsl
    fun engines(vararg types: Engine) {
        engines = types.toList()
    }
}

internal class RestrictionConfigBuilder(private val defaultDirection: Direction? = null) {
    fun build(context: RestrictionConfigContext) = Restriction(
        direction = context.direction ?: defaultDirection,
        minFl = context.minFl ?: context.cruisingFl?.start,
        maxFl = context.maxFl ?: context.cruisingFl?.endInclusive,
        destinations = context.destinations,
        airways = context.airways,
        engines = context.engines,
    )
}
