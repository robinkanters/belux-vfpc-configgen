package org.beluxvacc.vpfcgen.internal

import org.beluxvacc.vpfcgen.api.VfpcConfigDsl
import org.beluxvacc.vpfcgen.model.AirportConfig
import org.beluxvacc.vpfcgen.model.Direction
import org.beluxvacc.vpfcgen.model.Engine
import org.beluxvacc.vpfcgen.model.Restriction

@VfpcConfigDsl
class SidContext {
    internal val restrictions = mutableListOf<Restriction>()
    internal var defaults: (RestrictionConfigContext.() -> Unit)? = null

    @VfpcConfigDsl
    fun defaults(block: RestrictionConfigContext.() -> Unit) {
        defaults = block
    }

    @VfpcConfigDsl
    fun config(block: RestrictionConfigContext.() -> Unit) {
        restrictions += RestrictionConfigBuilder().build(
            RestrictionConfigContext().apply { this@SidContext.defaults?.invoke(this) }.apply(block)
        )
    }
}

class SidConfigBuilder {
    fun build(context: SidContext) = context.restrictions.toList() + makeDefaultRestriction(context)

    private fun makeDefaultRestriction(context: SidContext) = RestrictionConfigBuilder().build(RestrictionConfigContext().apply {
        this.engines = Engine.All
    }.apply {
        context.defaults?.invoke(this)
    })
}

