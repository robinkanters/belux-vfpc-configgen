package org.beluxvacc.vpfcgen.internal

import org.beluxvacc.vpfcgen.api.VfpcConfigDsl
import org.beluxvacc.vpfcgen.model.Airport
import org.beluxvacc.vpfcgen.model.AirportConfig

@VfpcConfigDsl
object VfpcContext {
    internal val configs = mutableListOf<AirportConfig>()

    @VfpcConfigDsl
    fun airport(airport: Airport, block: AirportContext.() -> Unit) {
        configs.add(AirportConfigBuilder().build(AirportContext(airport).apply(block)))
    }

    @VfpcConfigDsl
    operator fun Airport.invoke(block: AirportContext.() -> Unit) = airport(this, block)
}

class VfpcContextBuilder {
    fun build(context: VfpcContext) = context.configs
}
