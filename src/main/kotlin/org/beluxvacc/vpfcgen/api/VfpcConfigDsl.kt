package org.beluxvacc.vpfcgen.api

import org.beluxvacc.vpfcgen.internal.VfpcContext
import org.beluxvacc.vpfcgen.internal.VfpcContextBuilder

@VfpcConfigDsl
fun vfpc(block: VfpcContext.() -> Unit) = VfpcContextBuilder().build(VfpcContext.apply(block))

fun test() {
    vfpc {
    }
}

@DslMarker
@Retention(AnnotationRetention.SOURCE)
annotation class VfpcConfigDsl
