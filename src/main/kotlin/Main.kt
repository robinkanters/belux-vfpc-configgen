import kotlinx.cli.ArgParser
import kotlinx.cli.ArgType
import kotlinx.cli.default
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.json.Json
import org.beluxvacc.vpfcgen.model.AirportConfig
import java.io.File

fun main(args: Array<String>) {
    val parser = ArgParser("vfpcconfiggen")
    val output by parser.option(ArgType.String, shortName = "o", description = "Output file name")
    val pretty by parser.option(ArgType.Boolean, shortName = "p", description = "Output pretty JSON").default(true)

    parser.parse(args)

    val s = Json {
        encodeDefaults = false
        prettyPrint = pretty
    }.encodeToString(ListSerializer(AirportConfig.serializer()), config())
    println(s)

    output?.let { o ->
        File(o).writeText(s)
    }
}
