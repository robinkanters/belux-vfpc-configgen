@file:Suppress("DuplicatedCode")

import org.beluxvacc.vpfcgen.api.vfpc
import org.beluxvacc.vpfcgen.model.Airport.BRUSSELS
import org.beluxvacc.vpfcgen.model.Direction.EVEN
import org.beluxvacc.vpfcgen.model.Direction.ODD

fun config() = vfpc {
    BRUSSELS {
        "LNO" {
            defaults {
                direction = ODD
            }
        }
        "CIV" {
            defaults {
                direction = ODD
            }

            config {
                maxFl = 195
                destinations("LFP", "LFOB")
            }
        }
        "KOK" {
            defaults {
                direction = EVEN
            }

            config {
                maxFl = 290
                airways("UL610")
                destinations("EGKK")
            }
            config {
                maxFl = 390
                destinations("EGLL", "EGWU")
                airways("UL608")
            }
            config {
                maxFl = 280
                destinations("EGSS", "EGGW", "EGSC", "EGSH", "EGLC", "EGKB", "EGMC", "EGTO")
                airways("UL608")
            }
            config {
                minFl = 195
                airways("UL607")
            }
            config {
                cruisingFl = 45..195
                airways("L607")
            }
        }
        "HELEN" {
            defaults {
                direction = EVEN
            }

            config {
                maxFl = 215
                destinations("EGGW", "EGKB", "EGLC", "EGMC", "EGSC", "EGSS", "EGTC", "EGTO")
            }
            config {
                maxFl = 245
                destinations("EGLL", "EGVA", "EGVN", "EGWU", "EGBJ")
            }
            config {
                cruisingFl = 140..140
                destinations("EHAM")
            }
            config {
                direction = ODD
                airways("N873", "Z708", "T606")
            }
        }
        "DENUT" {
            defaults {
                direction = EVEN
            }

            config {
                maxFl = 215
                destinations("EGGW", "EGKB", "EGLC", "EGMC", "EGSC", "EGSS", "EGTC", "EGTO")
            }
            config {
                maxFl = 245
                destinations("EGLL", "EGVA", "EGVN", "EGWU", "EGBJ")
            }
            config {
                cruisingFl = 140..140
                destinations("EHAM")
            }
        }
        "NIK" {
            defaults {
                direction = EVEN
            }

            config {
                cruisingFl = 140..140
                destinations("EHAM")
            }
            config {
                direction = ODD
                airways("N873", "Z708", "T606")
            }
        }

        listOf("ELSIK", "SPI", "PITES", "ROUSY", "SOPOK").forEach {
            it {
                defaults {
                    direction = ODD
                }
            }
        }
    }
}
